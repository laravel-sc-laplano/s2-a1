<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function hello(){
        //return view('hello')-> with('name','Homer Siphon');
        //return view('hello',['name' => 'Homer Simpson']);

        $info = array(
            'frontend' => 'Zuitt Coding Bootcamp',
            'topics' => ['HTML','JS','REACT']

        );
        return view('hello')->with($info);
    }

    public function about(){
        return view('pages/about');
    }


    public function services(){
        return view('pages/services');
    }
}
